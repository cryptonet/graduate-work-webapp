FROM nginx:1.23.1-alpine

MAINTAINER Aleksandr Khaikin <aleksandr.devops@gmail.com>

COPY index.html /usr/share/nginx/html/

HEALTHCHECK --interval=5s --retries=2 --start-period=2s --timeout=2s CMD curl http://localhost:80 || exit 1

EXPOSE 80 443
