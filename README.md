# Web application for graduate work of Netology.

## Description
The simple web page only for test infrastructure.

## Quick start

### Simple way
Just open `index.html` in any modern browser.

### Hard way (run in `docker`)
**NOTE: [docker](https://www.docker.com/) must be installed!**

#### 1. Create image
```shell
docker build --tag webapp .
```
#### 2. Run container
```shell
docker run --rm --name webapp --publish 80:80 webapp .
```

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
